// config/database.js
"use strict";
var pg = require('pg');
var url = 'postgres://postgres:root@localhost:5432/bank';
var client = new pg.Client(url);
client.connect(function(err) {
	if(err) {
		return console.error('could not connect to postgres', err);
	}})

module.exports = {

	'client' : client


};