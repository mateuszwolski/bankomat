
// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

var db = require('./database');

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        var idTab = id.split("/");
        db.client.query('SELECT *  FROM customer_bank_account_t where account_id = '+idTab[0] + ' and customer_id = '+idTab[1],
            function(err, user) {
            if(err) {
                return console.error('error running query', err);
            }
                var uu = {username:id, pin:user.rows[0].pin};
            done(err, uu);

        });
    });


    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'pin',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, pin, done) { // callback with email and password from our form

        var idTab = username.split("/");
        db.client.query('SELECT *  FROM customer_bank_account_t where account_id = '+idTab[0] + ' and customer_id = '+idTab[1],
            function(err, user) {

                if (err){
                    return done(err);
                }
                if (!user) {
                    return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                }
                if (pin != user.rows[0].pin) {

                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                }
                var uu = {id:username, pin:pin};
                // all is well, return successful user
                return done(null, uu);
                //output: Tue Jan 15 2013 19:12:47 GMT-600 (CST)

            });
    }));

};
