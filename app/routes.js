// app/routes.js
var db = require('../config/database');

module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('index.ejs'); // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));


	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {


		var idTab = req.user.username.split("/");
		db.client.query('SELECT accountInfo('+idTab[0]+')',
			function(err, info) {
				if(err) {

					return console.error('error running query', err);
				}

				var zm = info.rows[0].accountinfo;
				var idTab2 = zm.split(",");
				var splitData = idTab2[0].split('.');

				res.render('profile.ejs', {
					user : req.user, // get the user out of session and pass to template
					data : splitData[0],
					saldo: idTab2[1],
					dostepne: idTab2[2]
				});

			});


	});

	app.get('/withdrawal/:id',isLoggedIn, function(req, res) {
		var atmId = req.params.id;

		res.render('withdrawal.ejs', {
			atmid : atmId,
			user : req._passport.session.user
				});
	});

	app.post('/withdrawal',isLoggedIn, function(req, res) {
		var money = req.body.money;
		var atmid = req.body.atmid;
		var user = req._passport.session.user;
		var userTab = user.split("/");

		db.client.query('SELECT getCash('+userTab[0]+' , '+atmid+' , '+money +')',
			function(err, info) {
				if(err) {
					return console.error('error running query', err);
				}
				var row = info.rows[0].getcash;
				var result = row.split("|")[0];
				if(result != 0){
					var errorInfo;
					if(result == 3){
						errorInfo = 'brak środków na koncie!';
					}
					else if(result ==2)
					{
						errorInfo = 'brak środków w banomacie!';
					}
					else if(result ==1)
					{
						errorInfo = 'liczba nie jest podzielna przez 50!';
					}
					else if(result ==4)
					{
						errorInfo = 'brak banknotów 50 zł!';
					}
					res.render('summation.ejs', {
						error : errorInfo,
						user : user,
						atmid : atmid,
						cashTable : null,
						money : money
					});
				}else {
					var cash = row.split("|")[1].split(',');
					var cashTable = [];

					for(var i =0;i<cash.length;i++){
						var xx = cash[i].split('x');
						cashTable.push([xx[0], xx[1]]);
					}

					res.render('summation.ejs', {
						atmid : atmid,
						user : user,
						money : money,
						cashTable : cashTable,
						error : null
					});
				}
			});
	});


	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.get('/atms', isLoggedIn, function(req, res) {
		db.client.query('SELECT *  FROM atm_t',
			function(err, atms) {
				if(err) {
					return console.error('error running query', err);
				}
				res.render('atms.ejs', {
					atms : atms.rows // get the user out of session and pass to template
				});

			});

	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}

